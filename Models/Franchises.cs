﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Models
{
    public class Franchises
    {

        [Required]
        [Key]
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string FranchiseName { get; set; }
        public string Description { get; set; }

        public ICollection<Movies> Movies { get; set; }
    }
}
