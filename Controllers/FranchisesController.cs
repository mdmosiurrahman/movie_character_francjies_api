﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;

namespace MovieCharacterApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;

        public FranchisesController(MovieCharacterDbContext context)
        {
            _context = context;
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Franchises>>> GetFranchises()
        {
            return await _context.Franchises.ToListAsync();
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Franchises>> GetFranchises(int id)
        {
            var franchises = await _context.Franchises.FindAsync(id);

            if (franchises == null)
            {
                return NotFound();
            }

            return franchises;
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchises(int id, Franchises franchises)
        {
            if (id != franchises.FranchiseId)
            {
                return BadRequest();
            }

            _context.Entry(franchises).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchisesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Franchises>> PostFranchises(Franchises franchises)
        {
            _context.Franchises.Add(franchises);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchises", new { id = franchises.FranchiseId }, franchises);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchises(int id)
        {
            var franchises = await _context.Franchises.FindAsync(id);
            if (franchises == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchises);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchisesExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
    }
}
